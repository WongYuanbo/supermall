const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}

// module.exports ={
//     configureWebpack:{
//         resolve:{
//             alias:{
//                 // '@':resolve('src'),
//                 'assets': '@/assets',
//                 'common': '@/common',
//                 'network': '@/network',
//                 'components':'@/components'
//             }
//         }
//     }
// }
module.exports = {
    chainWebpack: config => {
      config.resolve.alias
        .set("@", resolve("src"))
        .set("assets", resolve("src/assets"))
        .set("components", resolve("src/components"))
    },
  }