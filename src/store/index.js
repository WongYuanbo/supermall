import { resolve } from 'path';
import { createStore } from 'vuex'
// vue中没有导出set方法
// import {set} from 'vue'

export default createStore({
  state: {
    imgLoadTimes:0,
    cartProdutionInfo:[]
  },
  mutations: {
    imgLsitLoad(state,oneTime){
      state.imgLoadTimes =state.imgLoadTimes+oneTime;
    },
    addProduct(state,payLoad){
      // 注意：如果方法中只执行一句代码时可以不用大括号和return，如果用了大括号则必须加上return
      // let productTem = state.cartProdutionInfo.find(item => item.id === payLoad.id)
      let productTem = state.cartProdutionInfo.find(item => 
         {return item.id === payLoad.id})
      if(productTem){
        productTem.count +=1;
      }
      else{
        state.cartProdutionInfo.push(payLoad);
      }
    },
    ADD_Counter(state,payLoad){
        payLoad.count +=1;
    },
    ADD_NewProduct(state,payLoad){
      state.cartProdutionInfo.push(payLoad);
    },

    selectAll(state,payLoad)
    {
        for(let i=0;i<state.cartProdutionInfo.length;i++)
        {
          if(state.cartProdutionInfo[i].checked !== payLoad)
          {
            state.cartProdutionInfo[i].checked = payLoad;
          }
        }
    }

  },
  actions: {
     addProductAction(context,payLoad){
      return new Promise((resolve,reject)=>{
        let productTem = context.state.cartProdutionInfo.find(item => 
          {return item.id === payLoad.id})
          if(productTem){
             context.commit("ADD_Counter",productTem);
             resolve('当前商品数量加一');
          }
          else
          {
            context.commit("ADD_NewProduct",payLoad);
            resolve('添加新商品');
          }
      })
     }
  },
  modules: {
  },
  getters:{
    productNum:state =>{
      return state.cartProdutionInfo.length
    },
    cartProdutInfo(state){
      return state.cartProdutionInfo;
    }
  }
})
