import {request} from './request';

export function getHomeMutlData(){
   return request(
       {
         url:"/home/multidata",
         methond:"get"
       }
   )
}

export function getHomeGoodsData(type,page) {
  return request(
    {
      url:"/home/data",
      methond:"get",
      params:{
        type,
        page
      }
    }
  )
  
}