import axios from 'axios'

export function request (config){
  const service = axios.create({
      // baseURL:'http://123.207.32.32:8000',
      baseURL: 'http://152.136.185.210:7878/api/m5',
      timeout:5000
  })
   //方法一:将请求服务包在Promise中，通过service实例的.then方法，将相关信息返回
//    return new Promise((resolve,reject) => 
//    {
//        service(config)
//        .then(res => resolve(res))
//        .catch(err=> reject(err));    
//     })
// 方法二：其实service的返回值就是axiosPromise类型，本身就有是Promise类型对象，
// 方法一将其包在promise中是多此一举的。


//请求拦截——在发送请求时进行拦截，对相应的请求信息进行判断和处理后，
// 再将请求信息返回给请求实例进行请求操作
 service.interceptors.request.use(config1=>
    {
      //请求config处理
      // console.log(config1);
      return config1;
    },err=>{
       console.log(err);
       return err
    }
    )
//响应拦截
service.interceptors.response.use(
    response=>{
      //对请求到的服务器相应的结构进行判断和处理
      // console.log(response);
      return response.data; 
    },
    error=>{
        console.log(error)
        return error
    }
)

return service(config);
}
