import { createApp,h, ref } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
 import toast from './components/common/toast';


createApp(App).use(store).use(router).mount('#app')
// createApp(App).use(store).use(router).use(toast).mount('#app')
