import toastNew from './ToastNew.vue';
import {createVNode, render} from 'vue';
//import type {App} from 'vue';
import App from '@/App.vue'


export default{
    install(App){
        const container = document.createElement('div');
        const vm = createVNode(toastNew);
        render(vm,container)
        document.body.appendChild(container);
        App.config.globalProperties.$toastCom = container;
    }
}

// const obj = {};
// obj.install = function (Vue) {
//     // console.log(Vue);
//     // // 1.创建组件构造器------vue3.x 不支持extend
//     // const toastContructor = Vue.extend(toastNew)
//     // // 2.通过new的方式，根据组件构造器， 创建一个组件对象
//     // const toastCom = new toastContructor();

//     // // 3.将创建的组件对象，手动挂载到某一个元素上

//     // toastCom.$mount(document.createElement('div'));

//     // // 4.组件对象对应的.$el 就是div，再将其添加到body上
//     // document.body.appendChild(toastCom.$el);

//     // Vue.prototype.$toastCom =toastCom;

// }
// export default obj;