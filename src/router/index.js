import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path:'/',
    redirect:'/home'
  },
  {
    name:'home',
    path:'/home',
    component:()=>import('../views/home/home.vue'),
    meta:{
      //  keepAlive: true //设置页面是否需要使用缓存,好像设置不设置都没什么用
    }
  },
  {
    path:'/category',
    component:()=>import('../views/category/category.vue'),
    meta:{
      // keepAlive: true //设置页面是否需要使用缓存
    }
  },
  {
    path:'/shopingCart',
    component:()=>import('../views/shopingCart/shopingCart.vue'),
    meta:{
      // keepAlive: true //设置页面是否需要使用缓存
    }
  },
  {
    path:'/profile',
    component:()=>import('../views/profile/profile.vue'),
    meta:{
      // keepAlive: true //设置页面是否需要使用缓存
    }
  },
  {
    name:'detail',
    path:'/detail',
    component:()=>import ('../views/detail/Detail.vue'),
    meta:{
      //  keepAlive: false //设置页面是否需要使用缓存
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
